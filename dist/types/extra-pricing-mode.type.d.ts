/**
 * The ExtraPricingMode type determines how the prices of the extra will be handled and displayed.
 *
 * "Additional" - An included extra whose price should not be included within the rate price. When the
 * 'displayInclusiveExtrasAsTaxes' feature is enabled for a property, additional included extra prices
 * will be displayed as taxes, separate to the rate prices.
 *
 * "Inclusive" - An included extra whose price should be included within the rate price.
 *
 * "Non-Inclusive" - The default, standard extra that is not included in any rates.
 */
export declare type ExtraPricingMode = 'Additional' | 'Inclusive' | 'Non-Inclusive';
