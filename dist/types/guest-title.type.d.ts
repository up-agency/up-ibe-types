export declare type GuestTitle = 'Mr' | 'Ms' | 'Dr' | 'Prof' | 'Other' | undefined;
