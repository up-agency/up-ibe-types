export declare type TaxType = 'VAT' | 'TouristTax' | 'CityTax' | 'SalesTax' | 'ServiceCharge';
