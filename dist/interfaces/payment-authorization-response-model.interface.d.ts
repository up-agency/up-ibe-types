export interface PaymentAuthorisationResponseModel {
    bookingRequestId: string;
    authorisation: string;
}
