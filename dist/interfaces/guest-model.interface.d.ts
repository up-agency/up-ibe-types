import { AddressModel } from "./address-model.interface";
import { GuestTitle } from "../types/guest-title.type";
export interface GuestModel {
    title?: GuestTitle;
    middleInitial?: string;
    firstName?: string;
    lastName: string;
    email?: string;
    phone?: string;
    address?: AddressModel;
    company?: {
        name?: string;
        taxId?: string;
    };
}
