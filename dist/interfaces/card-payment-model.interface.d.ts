export interface CardPaymentModel {
    cardNumber: string;
    cardName: string;
    expiryMonth: string;
    expiryYear: string;
    paymentMethod: string;
    payerEmail?: string;
    token?: string;
    resultCode?: string;
}
