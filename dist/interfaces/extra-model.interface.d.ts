import { MonetaryValueModel } from "./monetary-value-model.interface";
import { TaxModel } from "./tax-model.interface";
import { Image } from "./image.interface";
import { PricingUnit } from "../types/pricing-unit.type";
import { ExtraPricingMode } from "../types/extra-pricing-mode.type";
export interface ExtraModel {
    id: string;
    name: string;
    description: string;
    quantity?: number;
    pricingMode: ExtraPricingMode;
    totalGrossAmount?: MonetaryValueModel;
    prePaymentAmount?: MonetaryValueModel;
    grossAmount?: MonetaryValueModel;
    pricingUnit?: PricingUnit;
    taxes?: TaxModel[];
    image?: Image;
    isInclusiveInRate?: boolean;
}
