import { MonetaryValueModel } from "./monetary-value-model.interface";
export interface ReservationCancellationFeeModel {
    name: string;
    description: string;
    fee?: MonetaryValueModel;
    dueDateTime?: Date;
}
