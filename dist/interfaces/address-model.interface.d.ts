export interface AddressModel {
    addressLine1?: string;
    addressLine2?: string;
    city?: string;
    postalCode?: string;
    countryCode?: string;
    state?: string;
}
