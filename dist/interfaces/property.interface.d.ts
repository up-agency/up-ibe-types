import { PropertyPaymentProviderSettings } from "./property-payment-provider-settings.interface";
import { PaymentProviderSettings } from "./payment-provider-settings.interface";
import { UnitType } from "./unit-type.interface";
import { Location } from "./location.interface";
import { Image } from "./image.interface";
export interface PropertyConfig {
    displayInclusiveExtrasAsTaxes?: boolean;
}
export interface Property {
    pmsId: string;
    id: number;
    name: string;
    config: PropertyConfig;
    images: Image[];
    unitTypes: UnitType[];
    location?: Location;
    paymentProvider: string;
    paymentProviderSettings: PaymentProviderSettings;
    propertyPaymentProviderSettings: PropertyPaymentProviderSettings;
}
