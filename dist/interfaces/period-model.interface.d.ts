export interface PeriodModel {
    hours?: number;
    days?: number;
    months?: number;
}
