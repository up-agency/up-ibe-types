export interface CreateBookingRequestResponseModel {
    bookingRequestId: string;
    paymentSetupData: any;
    paymentRequired: boolean;
    prePaymentAmount: number;
    currency: string;
}
