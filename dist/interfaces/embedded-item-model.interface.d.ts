export interface EmbeddedItemModel {
    readonly id: string;
    name: string;
    pmsDescription?: string;
    customDescription?: string;
}
