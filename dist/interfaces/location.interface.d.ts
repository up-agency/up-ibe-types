export interface Location {
    addressLine1: string;
    postalCode: string;
    city: string;
    countryCode: string;
}
