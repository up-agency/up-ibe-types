import { MonetaryValueModel } from './monetary-value-model.interface';
export interface CancellationFeeModel {
    name: string;
    description: string;
    dueDateTime: string;
    fee: MonetaryValueModel;
}
