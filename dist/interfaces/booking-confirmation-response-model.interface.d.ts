import { BookingModel } from "./booking-model.interface";
export interface BookingConfirmationResponseModel {
    success: boolean;
    bookingRequestId: string;
    propertyId: string;
    bookings: BookingModel[];
}
