export interface MonetaryValueModel {
    amount: number;
    currency: string;
}
