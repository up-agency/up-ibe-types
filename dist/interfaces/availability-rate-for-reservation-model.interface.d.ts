import { AvailabilityResultRateModel } from "./availability-result-rate-model.interface";
export interface AvailabilityRateForReservationModel extends AvailabilityResultRateModel {
    roomTypeId: string;
}
