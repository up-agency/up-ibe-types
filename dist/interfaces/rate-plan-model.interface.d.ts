import { DepositPolicy } from "./deposit-policy.interface";
import { UnitTypeModel } from "./unit-type-model.interface";
import { GuaranteeType } from "../types/guarantee.type";
export interface RatePlanModel {
    id: string;
    name: string;
    description: string;
    unitType?: UnitTypeModel;
    guaranteeType?: GuaranteeType;
    depositPolicy?: DepositPolicy;
}
