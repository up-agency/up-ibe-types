import { TermsAndConditions } from './terms-and-conditions.interface';
import { CheckoutFields } from "./checkout-fields.interface";
export interface Settings {
    defaultAddressCountryCode: string;
    defaultLanguage: string;
    paymentStepEnabled: boolean;
    termsAndConditionsUrls: TermsAndConditions;
    primaryColour: string;
    availabilityCalendarEnabled: boolean;
    displayPropertiesByRegionEnabled: boolean;
    childrenOptionEnabled: boolean;
    advancedPropertySelectorEnabled: boolean;
    onlyShowPromoCodeRatesEnabled: boolean;
    maxNumberOfAdults: number;
    numberOfAdultsDefault: number;
    maxNumberOfChildren: number;
    roomQtySelectorLimit: number;
    showCityTaxEstimateEnabled: boolean;
    showServiceChargeEnabled: boolean;
    startDateEnabled: boolean;
    startDate: Date | undefined;
    guestManagementEnabled: boolean;
    sendGuestEmailsEnabled: boolean;
    checkoutFields: CheckoutFields;
    autoscrollEnabled: boolean;
    availabilityCalendarRestrictionsModalEnabled: boolean;
}
