export interface IbeServerApiResponseModel {
    success: boolean;
    message: string;
}
