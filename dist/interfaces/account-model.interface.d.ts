export interface AccountModel {
    code: string;
    name: string;
    description: string;
}
