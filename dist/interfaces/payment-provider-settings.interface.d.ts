import { CardCodes } from "./card-codes.interface";
export interface PaymentProviderSettings {
    paymentProviderSettingsPerPropertyEnabled: boolean;
    testModeEnabled: boolean;
    checkoutApiKey?: string;
    checkoutApiUrlPrefix?: string;
    paypalEnabled?: boolean;
    accountId?: string;
    password?: string;
    username?: string;
    customerId?: string;
    terminalId?: string;
    publicKey?: string;
    secretKey?: string;
    threeDSEnabled?: string;
    idealEnabled?: boolean;
    cardCodes?: CardCodes;
}
