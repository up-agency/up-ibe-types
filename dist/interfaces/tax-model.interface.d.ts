import { TaxType } from "../types/tax.type";
export interface TaxModel {
    id?: string;
    type: TaxType;
    percent?: number;
    amount: number;
    currency: string;
    name?: string;
}
