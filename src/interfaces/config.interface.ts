import { Property } from "./property.interface";
import { Settings } from "./settings.interface";

export interface Config {
  settings: Settings;
  properties: Property[];
  pmsProvider: string;
  accountPaymentProvider: string;
  accountPaypalEnabled: boolean;
  accountIdealEnabled: boolean;
  accountTestModeEnabled: boolean;
}