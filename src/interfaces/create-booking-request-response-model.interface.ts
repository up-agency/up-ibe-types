export interface CreateBookingRequestResponseModel {
  bookingRequestId: string;
  // tslint:disable-next-line:no-any
  paymentSetupData: any;
  paymentRequired: boolean;
  prePaymentAmount: number;
  currency: string;
}