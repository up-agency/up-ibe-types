import { MonetaryValueModel } from "./monetary-value-model.interface";
import { EmbeddedItemModel } from "./embedded-item-model.interface";

export interface AvailabilityResultByRegionModel {
  property: {
    id: string,
    code: string,
    name: string,
    description: string,
    location: {
      addressLine1: string,
      addressLine2?: string,
      postalCode: string,
      city: string,
      countryCode: string,
    }
  };
  fromPrice: MonetaryValueModel;
  unitType: EmbeddedItemModel;
}