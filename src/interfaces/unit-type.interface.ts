import { Image } from "./image.interface";

export interface UnitType {
  id: number;
  pmsId: string;
  images: Image[];
  defaultRatePlanPmsId: string;
}