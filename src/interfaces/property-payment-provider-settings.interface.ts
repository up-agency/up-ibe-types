import { CardCodes } from "./card-codes.interface";

export interface PropertyPaymentProviderSettings {
  merchantAccount: string;
  subMerchantId?: string;
  cashierNumber?: string;
  workstationId?: string;
  paymentCurrency?: string;
  cardCodes?: CardCodes;
  additionalAdyenShopperDetailsEnabled?: boolean;
  riskProfileReference?: string;
}