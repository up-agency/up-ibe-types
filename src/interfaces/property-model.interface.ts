export interface PropertyModel {
  id: string;
  name: string;
  code: string;
  description: string;
  location: {
    addressLine1: string;
    postalCode: string;
    city: string;
    countryCode: string;
  }
}