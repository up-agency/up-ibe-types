export interface AmountModel {
  grossAmount: number;
  netAmount: number;
  currency: string;
}