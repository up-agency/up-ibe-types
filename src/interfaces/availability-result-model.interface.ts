import { AvailabilityResultRateModel } from "./availability-result-rate-model.interface";
import { MonetaryValueModel } from "./monetary-value-model.interface";
import { EmbeddedItemModel } from "./embedded-item-model.interface";

export interface AvailabilityResultModel {
  arrival: string;
  departure: string;
  featured: boolean;
  property: {
    id: string,
    code: string,
    name: string
  };
  unitType: EmbeddedItemModel;
  rates: AvailabilityResultRateModel[];
  fromPrice: MonetaryValueModel;
}