import { EmbeddedItemModel } from "./embedded-item-model.interface";
import { ReservationModel } from "./reservation-model.interface";

export interface ReservationDiffModel extends Partial<ReservationModel> {
  ratePlanToChange?: EmbeddedItemModel;
}