export interface UnitTypeModel {
  id: string;
  name: string;
  description: string;
}