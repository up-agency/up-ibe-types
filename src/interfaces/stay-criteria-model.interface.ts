export interface StayCriteriaModel {
  propertyId: string;
  arrival: string;
  departure: string;
  adults: number;
  childrenAges: number[];
  region?: string;
}