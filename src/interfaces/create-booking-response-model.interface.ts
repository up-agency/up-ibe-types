export interface CreateBookingResponseModel {
  pmsBookingIds: string[];
  bookingRequestId: string;
  success: boolean;
  message: string;
}