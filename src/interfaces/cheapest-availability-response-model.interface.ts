import { RestrictionModel } from "./restriction-model.interface";

export interface CheapestAvailabilityResponseModel {
  date: string;
  price: number;
  currency: string;
  isAvailable: boolean;
  restrictions?: RestrictionModel;
}