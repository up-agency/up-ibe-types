export interface TermsAndConditions {
  en: string;
  de: string;
  fr: string;
  ru: string;
  sv: string;
  fi: string;
  nl: string;
  es: string;
}