import { BookerModel } from "./booker-model.interface";
import { CardPaymentModel } from "./card-payment-model.interface";
import { ReservationModel } from "./reservation-model.interface";

export interface BookingModel {
  id?: string;
  reservations: ReservationModel[];
  booker: BookerModel;
  bookerComment?: string;
  paymentAccount?: CardPaymentModel;
}