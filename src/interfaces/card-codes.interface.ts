export interface CardCodes {
  [key: string]: string;
}