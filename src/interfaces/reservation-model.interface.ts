import { ReservationCancellationFeeModel } from "./reservation-cancellation-fee-model.interface";
import { MonetaryValueModel } from "./monetary-value-model.interface";
import { EmbeddedItemModel } from "./embedded-item-model.interface";
import { CardPaymentModel } from "./card-payment-model.interface";
import { ExtraModel } from "./extra-model.interface";
import { GuestModel } from "./guest-model.interface";
import { TaxModel } from "./tax-model.interface";

import { ReservationStatus } from "../types/reservation-status.type";
import { TravelPurpose } from "../types/travel-purpose.type";
import { GuaranteeType } from "../types/guarantee.type";

export interface ReservationModel {
  id?: string;
  bookingReference?: string;
  status?: ReservationStatus;
  arrival: string;
  departure: string;
  adults: number;
  childrenAges: number[];
  property: EmbeddedItemModel;
  ratePlan: EmbeddedItemModel;
  unitType: EmbeddedItemModel;
  guaranteeType?: GuaranteeType;
  extras: ExtraModel[];
  includedExtras?: ExtraModel[];
  cancellationFee?: ReservationCancellationFeeModel;
  totalBaseAmount?: MonetaryValueModel;
  totalGrossAmount: MonetaryValueModel;
  prePaymentAmount?: MonetaryValueModel;
  travelPurpose?: TravelPurpose;
  primaryGuest?: GuestModel;
  paymentAccount?: CardPaymentModel;
  taxes?: TaxModel[];
  promoCode?: string;
  corporateCode?: string;
  extrasAvailable?: boolean;
  estimatedCityTax?: TaxModel;
  estimatedServiceCharge?: TaxModel;
  companyId?: string;
  suppressedRate?: boolean;
  depositPolicy?: EmbeddedItemModel;
}