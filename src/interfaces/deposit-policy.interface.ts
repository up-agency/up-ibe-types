export interface DepositPolicy {
  id: number;
  name: string;
  description: string;
}