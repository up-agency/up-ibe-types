export interface Image {
  id: number;
  path: string;
  sort: number;
}