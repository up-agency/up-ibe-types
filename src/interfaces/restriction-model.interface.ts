export interface RestrictionModel {
  closed: boolean,
  closedOnArrival: boolean,
  closedOnDeparture: boolean,
  minLengthOfStay?: number,
  maxLengthOfStay?: number,
  minAdvancedBooking?: number,
  maxAdvancedBooking?: number
}