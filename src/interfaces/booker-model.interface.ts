import { AddressModel } from "./address-model.interface";

import { GuestTitle } from "../types/guest-title.type";

export interface BookerModel {
  title?: GuestTitle;
  middleInitial?: string;
  firstName?: string;
  lastName: string;
  email?: string;
  phone?: string;
  address?: AddressModel;
  preferredLanguage?: string;
  marketingConsent?: boolean;
  pmsGuestId?: string;
  company?: {
    name?: string;
    taxId?: string;
  }
}