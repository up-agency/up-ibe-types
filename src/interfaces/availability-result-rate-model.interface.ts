import { CancellationFeeModel } from "./cancellation-fee-model.interface";
import { MonetaryValueModel } from "./monetary-value-model.interface";
import { EmbeddedItemModel } from "./embedded-item-model.interface";
import { ExtraModel } from "./extra-model.interface";
import { TaxModel } from "./tax-model.interface";

import { GuaranteeType } from "../types/guarantee.type";

export interface AvailabilityResultRateModel {
  id: string;
  name: string;
  description: string;
  totalBaseAmount?: MonetaryValueModel;
  totalGrossAmount: MonetaryValueModel;
  availableUnits: number;
  isPromoRate?: boolean;
  isCorporateRate?: boolean;
  includedExtras?: ExtraModel[];
  taxes?: TaxModel[] | undefined;
  guaranteeType?: GuaranteeType;
  prePaymentAmount?: MonetaryValueModel;
  cancellationPolicy?: CancellationFeeModel;
  estimatedCityTax?: TaxModel;
  estimatedServiceCharge?: TaxModel;
  depositPolicy?: EmbeddedItemModel;
  suppressed?: boolean;
}