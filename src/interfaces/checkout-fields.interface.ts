export interface CheckoutFields {
  details: Record<
    'title' |
    'firstName' |
    'lastName' |
    'phone' |
    'email' |
    'password' |
    'travelPurpose' |
    'companyName' |
    'companyTaxId' |
    'guestComment',
    boolean
  >;
  address: Record<
    'street' |
    'city' |
    'postalCode' |
    'countryCode' |
    'state' |
    'marketingConsent',
    boolean
  >
}